import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SlideShow extends StatefulWidget {
  @override
  _SlideShowState createState() => _SlideShowState();
}

class _SlideShowState extends State<SlideShow> {
  int current = 0;
  List imgList = [
    "https://images.ladbible.com/resize?type=jpeg&url=http://beta.ems.ladbiblegroup.com/s3/content/a5d0cf975ed2302883ab29cdf068e207.png&quality=70&width=720&aspectratio=16:9&extend=white",
    "https://en.as.com/en/imagenes/2020/03/23/football/1584995050_666854_1584995092_noticia_normal.jpg",
    "https://ronaldo.com/wp-content/uploads/2020/05/GettyImages-1167403840-1174626668-1190283647.jpg"
  ];

  List<T> map<T>(List list,Function handler){
    List<T> result = [];
    for(var i=0;i<list.length;i++){
      result.add(handler(i,list[i]));
    }
    return result;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
              height: 40,
              width: double.infinity,
              padding: EdgeInsets.only(left: 5),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0)
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Text("Search here...",style: TextStyle(fontSize: 15,color: Colors.black26)),
                  ),
                  Expanded(
                      flex: 1,
                      child: Icon(Icons.search,color: Colors.black,)
                  )
                  /*Container(
                child: Icon(Icons.search),
              )*/
                ],
              )
          )
      ),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      padding: EdgeInsets.only(top: 0),
      child: Column(
        children: <Widget>[
          _slideShow()
        ],
      )
    );
  }

  _slideShow(){
    return Column(
      children: <Widget>[
        CarouselSlider(
          height: 200,
          initialPage: 0,
          enlargeCenterPage: true,
          autoPlay: true,
          reverse: false,
          autoPlayInterval: Duration(seconds: 5),
          onPageChanged: (index){
            setState(() {
              current = index;
            });
          },
          items: imgList.map((imgUrl){
            return Builder(
                builder:(BuildContext context){
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 3),
                    decoration: BoxDecoration(
                      color: Colors.green,
                    ),
                    child: Image.network(
                      imgUrl,
                      fit: BoxFit.fill,
                    ),
                  );
                }
            );
          }).toList(),
        ),
        SizedBox(height: 2),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(imgList,(index,url){
            return Container(
              width: 7,
              height: 7,
              margin: EdgeInsets.symmetric(vertical: 10,horizontal: 2),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: current   == index ? Colors.blue : Colors.black
              ),
            );
          }),
        )
      ],
    );
  }
}
