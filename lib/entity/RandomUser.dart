class RandomUser{
  List<Results> listResult;
  Info info;

  RandomUser({this.listResult,this.info});
  factory RandomUser.mapJson(Map map){
    List list = map['results'];
    return RandomUser(
      listResult: List.from(map["results"].map((x) => Results.mapJson(x))),
      info: Info.mapJson(map['info'])
    );
  }
}

class Results{
  String gender;
  String email;
  Name name;
  Results({this.gender,this.email,this.name});
  factory Results.mapJson(Map map){
    return Results(gender: map['gender'],email: map['email'],name: Name.mapJson(map['name']));
  }
}

class Name{
  String title;
  String first;
  String last;

  Name({this.title, this.first, this.last});
  factory Name.mapJson(Map map){
    return Name(
      title: map['title'],
      first: map['first'],
      last: map['last']
    );
  }
}

class Info{
  String seed;
  num results;
  num page;
  String version;

  Info({this.seed,this.results,this.page,this.version});
  factory Info.mapJson(Map map){
    return Info(
      seed: map['seed'],
      results: map['results'],
      page: map['page'],
      version: map['version']
    );
  }

}