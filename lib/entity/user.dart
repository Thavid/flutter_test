import 'package:flutter/foundation.dart';

class User{
  int userId;
  int id;
  String title;
  String body;

  User({this.userId,this.id,this.title,this.body});

  factory User.fromJson(Map map){
    return User(userId: map['userId'],id: map['id'],title: map['title'],body: map['body']);
  }
}