import 'package:my_app/entity/movie.dart';

class Category{
  String title;
  List<Movie> listMovie;

  Category({this.title,this.listMovie});
}