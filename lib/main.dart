import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app/FuturePage.dart';
import 'package:my_app/list_data.dart';
import 'package:my_app/list_movie_page.dart';
import 'package:my_app/login_page.dart';
import 'package:my_app/random_user_api.dart';
import 'package:my_app/slideshow.dart';
import 'package:my_app/state_full_page.dart';
void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SlideShow(),
    );
  }
}
