import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app/custom_view/home_page.dart';
import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';

class StateFullPage extends StatefulWidget {
  @override
  _StateFullPageState createState() => _StateFullPageState();
}

class _StateFullPageState extends State<StateFullPage> {

  String appBarTitle = "Before Click";
  int _index = 0;
  Container child;
  HomePage homePage = new HomePage();
  List<Widget> pageList = List<Widget>();
  @override
  Widget build(BuildContext context) {
    //Callback method
    pageList.add(HomePage(
      deleteItem: (String v){
          print(v);
        },)
    );
    pageList.add(_buildBody());
    pageList.add(_cartPage());
    pageList.add(_accountPage());

    List<BottomNavigationBarItem> items = [
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
      BottomNavigationBarItem(icon: Icon(Icons.category), title: Text("Category")),
      BottomNavigationBarItem(icon: Icon(Icons.shopping_cart), title: Text("Cart")),
      BottomNavigationBarItem(icon: Icon(Icons.account_circle), title: Text("Account")),
    ];

    BottomNavigationBadge badger = new BottomNavigationBadge(
        backgroundColor: Colors.red,
        badgeShape: BottomNavigationBadgeShape.circle,
        textColor: Colors.white,
        position: BottomNavigationBadgePosition.topRight,
        textSize: 9);

    setState(() {
      items = badger.setBadge(items, "10", 2);
    });

    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 40,
          width: double.infinity,
          padding: EdgeInsets.only(left: 5),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Text("Search here...",style: TextStyle(fontSize: 15,color: Colors.black26)),
              ),
              Expanded(
                flex: 1,
                child: Icon(Icons.search,color: Colors.black,)
              )
              /*Container(
                child: Icon(Icons.search),
              )*/
            ],
          )
        )
      ),
      body: IndexedStack(
        index: _index,
        children: pageList,
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
            canvasColor: Colors.white,
            primaryColor: Colors.blue,
            textTheme: Theme
                .of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.black))),
        child: BottomNavigationBar(
          onTap: (newIndex) => setState(() => _index = newIndex),
          currentIndex: _index,
          type: BottomNavigationBarType.fixed,
          items: items
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Text(appBarTitle),
          RaisedButton(
            child: Text('Click', style: TextStyle(fontSize: 20)),
            onPressed: () {
              setState(() {
                appBarTitle = appBarTitle == 'Before Click' ? 'After Click' : 'Before Click';
              });
          },
          ),
        ],
      ),
    );
  }

  _cartPage(){
    return Container(
      color: Colors.red,
    );
  }

  _accountPage(){
    return Container(
      color: Colors.yellow,
    );
  }
}
