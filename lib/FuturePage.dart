import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'entity/user.dart';

class FuturePage extends StatefulWidget {
  @override
  _FuturePageState createState() => _FuturePageState();
}

Future<String> getString(){
  Future<String> data = Future.delayed(Duration(seconds: 10),() => "Hello");
  return data;
}

Future<List<User>> getDataUrl(String url) async{
  Response response = await Client().get(url);
  if(response.statusCode == 200)
    return compute(parseData,response.body);
  else throw Exception("Response Error");
}
List<User> parseData(String responseBody){
  List list = json.decode(responseBody);
  List<User> listUser = list.map((e) => User.fromJson(e)).toList();
  return listUser;
}

class _FuturePageState extends State<FuturePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      alignment: Alignment.center,
      color: Colors.yellow,
      padding: EdgeInsets.all(10),
      child: FutureBuilder<List<User>>(
          future: getDataUrl("https://jsonplaceholder.typicode.com/posts?userId=5"),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Container(
                child: _buildListView(snapshot.data),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
    );
  }

  _buildListView(List<User> data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context,index){
          return Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 10),
            child: ListTile(
              title: Text(data[index].title),
              subtitle: (Text(data[index].body)),
            ),
          );
        });
  }

}
