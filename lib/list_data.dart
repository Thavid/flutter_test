import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///D:/Projects/flutter/my_app/lib/custom_view/customer_item_view.dart';
import 'package:my_app/entity/customer.dart';

class ListData extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text("List Data",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
      ),

      endDrawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Icon(Icons.account_circle),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
            ),
            ListTile(
              leading: Icon(Icons.email),
              title: Text("Email"),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("Setting"),
            )
          ],
        ),
      ),

      body: _buildBody(context),
    );
  }

  _buildBody(context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
        itemCount: _generateListData().length,
        itemBuilder:(context,index){
          return CustomerItemView.customerItemView(context, _generateListData()[index]);
        }
    );
  }

  _buildItemView(context,Customer item) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
        topLeft: Radius.circular(5),
        topRight: Radius.circular(5),
        bottomLeft: Radius.circular(5),
        bottomRight: Radius.circular(5)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 3,
           child: Container(
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.topRight,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(5)),
                image: DecorationImage(
                  image: NetworkImage(item.image,),
                  fit: BoxFit.fitWidth
                ),
              ),
            ),
          ),
         Expanded(
           flex: 7,
           child: Container(
             height: 140,
             width: MediaQuery.of(context).size.width,
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 Container(
                   padding: EdgeInsets.only(left: 10),
                   child: Text("Name :"+item.name),
                 ),
                 Container(
                   padding: EdgeInsets.only(left: 10),
                   child: Expanded(child: Text("Description :"+item.description, maxLines: 2, overflow: TextOverflow.ellipsis))
                 )
               ],
             ),
           ),
         )
        ],
      ),
    );
  }

  _generateListData(){
    String imageUrl = "https://photos1.iorbix.com/00/00/00/00/01/96/77/26/Marco-Reus-gvVV5LjJA-b.jpg";
    return [
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
      Customer(name:"Mina",image: imageUrl,description: "Gentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "GentlemanGentlemanGentlemanGentlemanGentlemanGentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
      Customer(name:"Thavid",image: imageUrl,description: "Gentleman"),
    ];
  }

}