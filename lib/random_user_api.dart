import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'entity/RandomUser.dart';
import 'network/radom_api.dart';
class RandomUserApi extends StatefulWidget {
  @override
  _RadomUserApiState createState() => _RadomUserApiState();
}
class _RadomUserApiState extends State<RandomUserApi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: FutureBuilder<dynamic>(
          future: getRandomUser(),
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.done)
              return Text(RandomUser.mapJson(snapshot.data).listResult[0].name.title);
            else
              return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
