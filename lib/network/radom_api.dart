import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:my_app/entity/RandomUser.dart';

Future<dynamic> getRandomUser() async{
  Response response = await Client().get("https://randomuser.me/api/?results=2");
  if(response.statusCode == 200)
    return compute(parseData,response.body);
  else
    print("error");
}

dynamic parseData(String responseBody){
  return json.decode(responseBody);
}