import 'package:flutter/material.dart';
import 'package:my_app/entity/Category.dart';
import 'package:my_app/entity/movie.dart';

class HomePage extends StatelessWidget{
  List<Category> listCategory = List();
  double maxHeight = 0;
  double maxWidth = 0;
  final VoidCallback voidCallback;
  final Function(String s) deleteItem;

  HomePage({this.voidCallback,this.deleteItem});

  @override
  Widget build(BuildContext context) {
    _calculateHeightGrid(context);
    _getDataMovie();
    return Container(
      /* child: SingleChildScrollView(*/
      color: Colors.grey[50],
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics:BouncingScrollPhysics(),
                  itemCount: _getDataMovie().length,
                  itemBuilder: (context,index){
                    return _movieCategoryItemView(context,_getDataMovie()[index]);
                  }
              ),
            ),
            SizedBox(height: 5),
            Container(
                padding: EdgeInsets.only(left: 6),
                child: Text("All Movies",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.black),)
            ),
            SizedBox(height: 5),
            Flexible(
              child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio:(maxWidth/maxHeight),
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  children: List.generate(listCategory[0].listMovie.length, (index) {
                    /*return Center(
                              child: Text("Item $index")*/
                    return _allMovieItemView(context,listCategory[0].listMovie[index]);
                  })
              ),
            )
          ],
          /*),*/
        ),
      ),
    );
  }

  _calculateHeightGrid(context){
    var size = MediaQuery.of(context).size;
    var itemHeight = (size.width/2) + (size.width*0.15);
    var height = size.height < 680 ? 0.157 : 0.13;
    var gridItemShowCount = ((size.height-AppBar().preferredSize.height)/itemHeight) - (((size.height-AppBar().preferredSize.height)/itemHeight)* height);
    maxHeight = (size.height-AppBar().preferredSize.height) / gridItemShowCount;
    maxWidth = size.width / 2;
    print(size.height);
  }

  _movieCategoryItemView(context,Category category) {
    return Container(
      height: 240,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      margin: EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(5),
              child: Text(category.title,style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.black))
          ),
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap : true,
                physics: BouncingScrollPhysics(),
                itemCount: category.listMovie.length,
                itemBuilder: (context,index){
                  return _movieItemView(category.listMovie[index]);
                }
            ),
          ),
        ],
      ),
    );
  }

  _movieItemView(Movie movie) {
    return GestureDetector(
      onTap: () => deleteItem("C0001"),
      child: Container(
        width: 130,
        height: 200,
        margin: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 130,
              height: 160,
              decoration: BoxDecoration(
                color: Colors.red,
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(movie.image),
                ),
              ),
            ),
            SizedBox(height: 5,),
            Container(
              height: 30,
              child: Text(movie.name,style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black,fontSize: 13),maxLines: 2),
            )
          ],
        ),
      ),
    );
  }

  _allMovieItemView(context,Movie movie) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width /2 + (MediaQuery.of(context).size.width*0.09),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.red,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(movie.image),
              ),
            ),
          ),
          SizedBox(height: 5),
          Container(
            height: 50,
            child: Text(movie.name,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 13),maxLines: 2),
          )
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300].withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
    );
  }

  _getDataMovie(){
    List<Movie> listMovies = [
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg")
    ];

    listCategory = [
      Category(title:"Korean Drama",listMovie: listMovies),
      Category(title:"Comedy",listMovie: listMovies)
    ];
    return listCategory;
  }
}