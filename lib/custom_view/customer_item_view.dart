import 'package:flutter/material.dart';
import 'package:my_app/entity/customer.dart';

class CustomerItemView{

  static customerItemView(context,item){
    return GestureDetector(
      onTap: (){
        print("Container clicked");
      },
      child: Container(
        height: 120,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                height: 100,
                width: 100,
                alignment: Alignment.topRight,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5)),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(item.image),
                  ),

                ),
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                height: 140,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text("Name :"+item.name),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text("Description :"+item.description),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}