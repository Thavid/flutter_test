import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app/ScrollBehavior.dart';
import 'package:my_app/test_page.dart';

class LoginPage extends StatelessWidget {
  var _username = TextEditingController();
  var _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      /*appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        title: Text("Login",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
      ),*/
      body: createBody(context),
    );
  }

  createBody(context) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              children: <Widget>[
                //Logo image
                Container(
                  margin: EdgeInsets.only(top: 40),
                  height: 100,
                  width: 100,
                  child: Image.asset("asset/image/logo_red.png"),
                ),

                //Title Login
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "Log In",
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontFamily: 'IndieFlower'),
                  ),
                ),

                //Input text field
                _createInputTextField("Please input username", "Username",
                    Icons.account_circle, _username, false),
                _createInputTextField("Please input password", "Password",
                    Icons.vpn_key, _password, true),
                _createButton(context),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text("Don't have account yet? SignUp here"),
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "Forget password",
                    style: TextStyle(
                        color: Colors.blue,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _createInputTextField(hint, label, iconData, textController, hide) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: TextField(
        controller: textController,
        obscureText: hide,
        decoration: InputDecoration(
            border: new OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.teal)),
            labelText: label,
            hintText: hint,
            prefixIcon: Icon(iconData)),
      ),
    );
  }

  _createButton(context) {
    return GestureDetector(
      onTap: () {
        if (_username.text == "thavid" && _password.text == "123") {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => TestPage(),
            ),
          );
        }else
          print("Wrong");
      },
      child: Container(
          margin: EdgeInsets.only(top: 20),
          width: double.infinity,
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.blue),
          child: Text("Login",
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold))),
    );
  }
}
