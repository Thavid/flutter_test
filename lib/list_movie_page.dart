import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app/custom_view/home_page.dart';
import 'package:my_app/entity/Category.dart';
import 'package:my_app/entity/customer.dart';
import 'package:my_app/entity/movie.dart';

class ListMoviePage extends StatelessWidget{
  List<Category> listCategory = List();
  double maxHeight = 0;
  double maxWidth = 0;
  HomePage homePage = HomePage();
  @override
  Widget build(BuildContext context) {
    _calculateHeightGrid(context);
   _getDataMovie();
   return Scaffold(
     appBar: AppBar(
       backgroundColor: Colors.red,
       centerTitle: true,
       actions: <Widget>[
         IconButton(icon:Icon(Icons.search), onPressed: () {  },),
         IconButton(icon:Icon(Icons.account_circle), onPressed: () {  },)
       ],
     ),
     drawer: Drawer(
       child: ListView(
         children: <Widget>[
           DrawerHeader(
             child: Icon(Icons.account_circle),
           ),
           ListTile(
             leading: Icon(Icons.home),
             title: Text("Home"),
           ),
           ListTile(
             leading: Icon(Icons.email),
             title: Text("Email"),
           ),
           ListTile(
             leading: Icon(Icons.settings),
             title: Text("Setting"),
           )
         ],
       ),
     ),
     body: _buildBody(context),
   );
  }

  _calculateHeightGrid(context){
    var size = MediaQuery.of(context).size;
    var itemHeight = (size.width/2) + (size.width*0.15);
    var height = size.height < 680 ? 0.157 : 0.13;
    var gridItemShowCount = ((size.height-AppBar().preferredSize.height)/itemHeight) - (((size.height-AppBar().preferredSize.height)/itemHeight)* height);
    maxHeight = (size.height-AppBar().preferredSize.height) / gridItemShowCount;
    maxWidth = size.width / 2;
    print(size.height);
  }

  _buildBody(context) {
    return Container(
       /* child: SingleChildScrollView(*/
          color: Colors.grey[50],
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics:BouncingScrollPhysics(),
                      itemCount: _getDataMovie().length,
                      itemBuilder: (context,index){
                        return _movieCategoryItemView(context,_getDataMovie()[index]);
                      }
                  ),
                ),
                SizedBox(height: 5),
                Container(
                    padding: EdgeInsets.only(left: 6),
                    child: Text("All Movies",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.black),)
                ),
                SizedBox(height: 5),
                Flexible(
                    child: GridView.count(
                        crossAxisCount: 2,
                        childAspectRatio:(maxWidth/maxHeight),
                        shrinkWrap: true,
                        physics: BouncingScrollPhysics(),
                        children: List.generate(listCategory[0].listMovie.length, (index) {
                          /*return Center(
                            child: Text("Item $index")*/
                          return _allMovieItemView(context,listCategory[0].listMovie[index]);
                        })
                    ),
                )
              ],
            /*),*/
        ),
      ),
    );
  }

  _movieCategoryItemView(context,Category category) {
    return Container(
      height: 240,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      margin: EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(5),
              child: Text(category.title,style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.black))
          ),
          Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap : true,
                physics: BouncingScrollPhysics(),
                itemCount: category.listMovie.length,
                itemBuilder: (context,index){
                  return _movieItemView(category.listMovie[index]);
                }
              ),
            ),
        ],
      ),
    );
  }

  _movieItemView(Movie movie) {
    return Container(
      width: 130,
      height: 200,
      margin: EdgeInsets.all(5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 130,
            height: 160,
            decoration: BoxDecoration(
              color: Colors.red,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(movie.image),
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: 30,
            child: Text(movie.name,style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black,fontSize: 13),maxLines: 2),
          )
        ],
      ),
    );
  }

  _allMovieItemView(context,Movie movie) {
    return Container(
      margin: EdgeInsets.all(5),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width /2 + (MediaQuery.of(context).size.width*0.09),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.red,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(movie.image),
              ),
            ),
          ),
          SizedBox(height: 5),
          Container(
            height: 50,
            child: Text(movie.name,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 13),maxLines: 2),
          )
        ],
      ),
    );
  }

  _getDataMovie(){
    List<Movie> listMovies = [
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg"),
      Movie(name: "Fast and FuriousFast and FuriousFast and Furious",image:"asset/image/movie2.jpg"),
      Movie(name: "Tom and Jerry",image:"asset/image/movie3.jpg"),
      Movie(name: "Ben Ten",image:"asset/image/movie4.jpg"),
      Movie(name: "Single Day",image:"asset/image/movie5.jpg"),
      Movie(name: "You are my memory",image:"asset/image/movie5.jpg")
    ];

    listCategory = [
      Category(title:"Korean Drama",listMovie: listMovies),
      Category(title:"Comedy",listMovie: listMovies)
    ];
    return listCategory;
  }
}
